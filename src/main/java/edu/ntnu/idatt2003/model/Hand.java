package edu.ntnu.idatt2003.model;

import edu.ntnu.idatt2003.contoller.Subject;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A class representing a hand of cards.
 */
public class Hand extends Subject {
  private List<Card> cards;

  /**
   * Constructor for a hand of cards.
   * @param cards The cards in the hand.
   */
  public Hand(List<Card> cards) {
    this.cards = cards;
  }

  /**
   * Deal a new hand of cards.
   * @param cards The new hand of cards.
   */
  public void dealNewHand(List<Card> cards) {
    this.cards = cards;
    notifyObservers();
  }

  /**
   * Get the cards in the hand.
   * @return The cards in the hand.
   */
  public List<Card> getCards() {
    return cards;
  }

  /**
   * Check if the hand is a flush.
   * @return True if the hand is a flush, false otherwise.
   */
  public boolean isFiveFlush() {
    return cards.stream()
        .collect(Collectors.groupingBy(Card::getSuit, Collectors.counting()))
        .values()
        .stream()
        .anyMatch(count -> count >= 5);
  }

  /**
   * Get the sum of the values of the cards in the hand.
   * @return The sum of the values of the cards in the hand.
   */
  public int sumOfValues() {
    return cards.stream()
        .mapToInt(Card::getValue)
        .sum();
  }

  /**
   * Get the hearts in the hand.
   * @return The hearts in the hand.
   */
  public String getHearts() {
    List<Card> hearts = cards.stream()
        .filter(card -> card.getSuit() == 'H')
        .toList();

    if (hearts.isEmpty()) {
      return "No hearts on hand";
    } else {
      return hearts.stream()
          .map(card -> "H" + card.getValue())
          .collect(Collectors.joining(" "));
    }
  }

  /**
   * Check if the hand has the queen of spades.
   * @return True if the hand has the queen of spades, false otherwise.
   */
  public boolean hasQueenOfSpades() {
    return cards.stream()
        .anyMatch(card -> card.getSuit() == 'S' && card.getValue() == 12);
  }
}
