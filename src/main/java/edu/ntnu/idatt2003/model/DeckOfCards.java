package edu.ntnu.idatt2003.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * A class representing a deck of cards.
 */
public class DeckOfCards {
  private final char[] suits = {'S', 'H', 'D', 'C'}; // Spades, Hearts, Diamonds, Clubs
  private List<Card> cards;

  /**
   * Constructor for a deck of cards.
   */
  public DeckOfCards() {
    cards = new ArrayList<>();
    for (char suit : suits) {
      for (int value = 1; value <= 13; value++) {
        cards.add(new Card(suit, value));
      }
    }
  }

  public List<Card> dealHand(int n) {
    if (n < 0 || n > cards.size()) {
      throw new IllegalArgumentException("Number of cards to deal must be between 0 and 52");
    }
    Collections.shuffle(cards, new Random());
    return new ArrayList<>(cards.subList(0, n));
  }
}
