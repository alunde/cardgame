package edu.ntnu.idatt2003.model;

/**
 * Class representing a card in a standard deck of cards.
 */
public class Card {
  private char suit;
  private int value;

  /**
   * Constructor for a card.
   * @param suit The suit of the card. 'S' for spades, 'H' for hearts, 'D' for diamonds, 'C' for clubs.
   * @param value The value of the card. 1 for ace, 2-10 for numbered cards, 11 for jack, 12 for queen, 13 for king.
   */
  public Card(char suit, int value) {
    this.suit = suit;
    this.value = value;
  }

  /**
   * Get the suit of the card.
   * @return The suit of the card.
   */
  public char getSuit() {
    return suit;
  }

  /**
   * Get the value of the card.
   * @return The value of the card.
   */
  public int getValue() {
    return value;
  }
}
