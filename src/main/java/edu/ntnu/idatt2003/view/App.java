package edu.ntnu.idatt2003.view;

import edu.ntnu.idatt2003.contoller.Observer;
import edu.ntnu.idatt2003.model.DeckOfCards;
import edu.ntnu.idatt2003.model.Hand;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class App extends Application implements Observer {
  private Label title;
  private Label handValue;
  private Label sumText;
  private Label sumValue;
  private Label queenText;
  private Label queenValue;
  private Label heartsText;
  private Label heartsValue;
  private Label flushText;
  private Label flushValue;
  private ComboBox<Integer> cardCountComboBox;
  private StackPane handLabelPane = new StackPane();
  private Hand hand;
  @Override
  public void start(Stage primaryStage) {
    DeckOfCards deckOfCards = new DeckOfCards();
    hand = new Hand(deckOfCards.dealHand(5));
    hand.attach(this);

    title = createLabelText("Card Game",40);
    handValue = createLabel(20);
    sumText = createLabelText("Sum of Values: ", 15);
    sumValue = new Label();
    queenText = createLabelText("Has Queen of Spades: ", 15);
    queenValue = new Label();
    heartsText = createLabelText("Hearts on Hand: ", 15);
    heartsValue = new Label();
    flushText = createLabelText("Is Flush: ", 15);
    flushValue = new Label();
    cardCountComboBox = new ComboBox<>();

    cardCountComboBox.setItems(FXCollections.observableArrayList(IntStream.rangeClosed(5, 10).boxed().collect(Collectors.toList())));
    cardCountComboBox.setValue(5);

    Button dealButton = createButton("Deal new hand", 15);
    dealButton.setOnAction(e -> {
      hand.dealNewHand(deckOfCards.dealHand(cardCountComboBox.getValue()));
      update();
    });

    handLabelPane = createStackPane(handValue, 50, 300, "lightgrey", "black", 20);

    StackPane sumValuePane = createStackPane(sumValue, 50, 10, "white", "black", 10);
    StackPane queenValuePane = createStackPane(queenValue, 100, 10, "white", "black", 10);
    StackPane heartsValuePane = createStackPane(heartsValue, 150, 10, "white", "black", 10);
    StackPane flushValuePane = createStackPane(flushValue, 150, 10, "white", "black", 10);

    HBox sum = createHBox(sumText, sumValuePane);
    HBox queen = createHBox(queenText, queenValuePane);
    HBox hearts = createHBox(heartsText, heartsValuePane);
    HBox flush = createHBox(flushText, flushValuePane);

    HBox root = new HBox();
    root.setSpacing(50);
    root.getChildren().addAll(sum, queen, hearts, flush);

    HBox deal = createHBox1(dealButton, cardCountComboBox);
    deal.setSpacing(10);



    GridPane gridPane = new GridPane();
    gridPane.add(title, 0, 0);
    gridPane.add(handLabelPane, 0,  1);
    gridPane.add(root, 0, 2);
    gridPane.add(deal, 2, 0);
    gridPane.setHgap(10);
    gridPane.setVgap(10);
    gridPane.setPadding(new Insets(30, 30, 30, 30));

    Scene scene = new Scene(gridPane);

    primaryStage.setScene(scene);
    primaryStage.setTitle("Card Game");
    primaryStage.setWidth(1300);
    primaryStage.setHeight(500);
    primaryStage.show();
  }


  /**
   * Create a label with a given font size.
   * @param fontSize The font size of the label.
   * @return The label.
   */
  private Label createLabelText(String text, int fontSize) {
    Label label = new Label(text);
    label.setStyle("-fx-font-size: " + fontSize + "px;");
    return label;
  }

  private Label createLabel(int fontSize) {
    Label label = new Label();
    label.setStyle("-fx-font-size: " + fontSize + "px;");
    return label;
  }

  /**
   * Create a button with a given text and font size.
   * @param text The text of the button.
   * @param fontSize The font size of the button.
   * @return The button.
   */
  private Button createButton(String text, int fontSize) {
    Button button = new Button(text);
    button.setStyle("-fx-font-size: " + fontSize + "px;");
    return button;
  }

  /**
   * Create a stack pane with a given label, width, height, color, border color and border radius.
   * @param label The label of the stack pane.
   * @param width The width of the stack pane.
   * @param height The height of the stack pane.
   * @param color The color of the stack pane.
   * @param borderColor The border color of the stack pane.
   * @param borderRadius The border radius of the stack pane.
   * @return The stack pane.
   */
  private StackPane createStackPane(Label label, int width, int height, String color, String borderColor,
                                    int borderRadius) {
    StackPane pane = new StackPane();
    pane.getChildren().add(label);
    pane.setStyle("-fx-background-color: " + color + "; -fx-alignment: center;" +
        "-fx-border-color: " + borderColor + ";" +
        "-fx-border-radius: " + borderRadius + ";" +
        "-fx-background-radius: " + borderRadius + ";");
    pane.setPrefSize(width, height);
    return pane;
  }

  /**
   * Create a horizontal box with a given label and stack pane.
   * @param label The label of the horizontal box.
   * @param stackPane The stack pane of the horizontal box.
   * @return The horizontal box.
   */
  private HBox createHBox(Label label, StackPane stackPane) {
    HBox hBox = new HBox();
    hBox.getChildren().addAll(label, stackPane);
    return hBox;
  }

  private HBox createHBox1(Button button, ComboBox comboBox) {
    HBox hBox = new HBox();
    hBox.getChildren().addAll(button, comboBox);
    return hBox;
  }

  /**
   * Check if the hand has the queen of spades.
   * @return "Yes" if the hand has the queen of spades, "No" otherwise.
   */
  private String QueenOfSpades() {
    if (hand.hasQueenOfSpades()) {
      return "Yes";
    } else {
      return "No";
    }
  }

  private String isFlush() {
    if (hand.isFiveFlush()) {
      return "Yes";
    } else {
      return "No";
    }
  }

  /**
   * The main method.
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }

    /**
     * Update the labels with the current hand.
     */
    @Override
    public void update() {
      String cardsString = hand.getCards().stream()
          .map(card -> card.getSuit() + String.valueOf(card.getValue()))
          .collect(Collectors.joining("        "));

      handValue.setText(cardsString);
      sumValue.setText("" + hand.sumOfValues());
      queenValue.setText("" + QueenOfSpades());
      heartsValue.setText("" + hand.getHearts());
      flushValue.setText("" + isFlush());
    }
}
