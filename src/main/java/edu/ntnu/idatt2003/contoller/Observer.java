package edu.ntnu.idatt2003.contoller;

public interface Observer {
  void update();
}
