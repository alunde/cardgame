package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.model.Card;
import edu.ntnu.idatt2003.model.Hand;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class HandTest {
  @Nested
  @DisplayName("Positive tests for the Hand class.")
  class PositiveHandTest {
    @Test
    public void testDealNewHandPositive() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      ));

      List<Card> newHand = Arrays.asList(
          new Card('S', 1),
          new Card('S', 2),
          new Card('S', 3),
          new Card('S', 4),
          new Card('S', 5)
      );

      hand.dealNewHand(newHand);

      assertEquals(newHand, hand.getCards());
    }

    @Test
    public void testGetCardsPositive() {
      List<Card> cards = Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      );

      Hand hand = new Hand(cards);

      assertEquals(cards, hand.getCards());
    }

    @Test
    public void testIsFiveFlushPositive() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      ));

      assertTrue(hand.isFiveFlush());
    }

    @Test
    public void testSumOfValuesPositive() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      ));

      assertEquals(15, hand.sumOfValues());
    }

    @Test
    public void testGetHeartsPositive() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('S', 3),
          new Card('H', 4),
          new Card('H', 5)
      ));

      String expected = "H1 H2 H4 H5";
      assertEquals(expected, hand.getHearts());
    }

    @Test
    public void testHasQueenOfSpadesPositive() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('S', 12), // Queen of Spades
          new Card('H', 4),
          new Card('H', 5)
      ));

      assertTrue(hand.hasQueenOfSpades());
    }
  }

  @Nested
  @DisplayName("Negative tests for the Hand class.")
  class NegativeHandTest {
    @Test
    public void testDealNewHandNegative() {
      List<Card> oldHand = Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      );

      Hand hand = new Hand(oldHand);

      List<Card> newHand = Arrays.asList(
          new Card('S', 1),
          new Card('S', 2),
          new Card('S', 3),
          new Card('S', 4),
          new Card('S', 5)
      );

      hand.dealNewHand(newHand);

      assertNotEquals(oldHand, hand.getCards());
    }

    @Test
    public void testGetCardsNegative() {
      List<Card> cards = Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      );

      Hand hand = new Hand(cards);

      List<Card> differentCards = Arrays.asList(
          new Card('S', 1),
          new Card('S', 2),
          new Card('S', 3),
          new Card('S', 4),
          new Card('S', 5)
      );

      assertNotEquals(differentCards, hand.getCards());
    }

    @Test
    public void testIsFiveFlushNegative() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('S', 2),
          new Card('D', 3),
          new Card('C', 4),
          new Card('H', 5)
      ));

      assertFalse(hand.isFiveFlush());
    }

    @Test
    public void testSumOfValuesNegative() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      ));

      assertNotEquals(16, hand.sumOfValues());
    }

    @Test
    public void testGetHeartsNegative() {
      Hand hand = new Hand(Arrays.asList(
          new Card('S', 1),
          new Card('S', 2),
          new Card('S', 3),
          new Card('S', 4),
          new Card('S', 5)
      ));

      assertEquals("No hearts on hand", hand.getHearts());
    }

    @Test
    public void testHasQueenOfSpadesNegative() {
      Hand hand = new Hand(Arrays.asList(
          new Card('H', 1),
          new Card('H', 2),
          new Card('H', 3),
          new Card('H', 4),
          new Card('H', 5)
      ));

      assertFalse(hand.hasQueenOfSpades());
    }
  }
}
