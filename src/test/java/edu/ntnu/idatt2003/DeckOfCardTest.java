package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.model.Card;
import edu.ntnu.idatt2003.model.DeckOfCards;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardTest {

  @Nested
  @DisplayName("Positive tests for the DeckOfCard class.")
  class PositiveDeckOfCardTest {
    @Test
    public void testDealHandPositive() {
      DeckOfCards deck = new DeckOfCards();
      int numberOfCards = 5;

      List<Card> hand = deck.dealHand(numberOfCards);

      assertEquals(numberOfCards, hand.size());
    }
  }

  @Nested
  @DisplayName("Negative tests for the DeckOfCard class.")
  class NegativeDeckOfCardTest {
    @Test
    public void testDealHandNegative() {
      DeckOfCards deck = new DeckOfCards();
      int numberOfCards = 53; // More than the number of cards in the deck

      assertThrows(IllegalArgumentException.class, () -> deck.dealHand(numberOfCards));
    }
  }
}
