package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.model.Card;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class CardTest {

  @Nested
  @DisplayName("Positive tests for the Card class.")

class PositiveCardTest {
    @Test
    public void testGetSuitPositive() {
      Card card = new Card('H', 1);

      assertEquals('H', card.getSuit());
    }

    @Test
    public void testGetValuePositive() {
      Card card = new Card('H', 1);

      assertEquals(1, card.getValue());
    }
  }

  @Nested
  @DisplayName("Negative tests for the Card class.")

  class NegativeCardTest {
    @Test
    public void testGetSuitNegative() {
      Card card = new Card('H', 1);

      assertNotEquals('S', card.getSuit());
    }

    @Test
    public void testGetValueNegative() {
      Card card = new Card('H', 1);

      assertNotEquals(2, card.getValue());
    }
  }
}
